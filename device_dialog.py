from flask import Flask, request, make_response, Response, jsonify
import os
import json
import sqlite3

from slackclient import SlackClient

## TODO (in order of priority)
# long-term database
# persistent script running on heroku
# make slash command accessible from any channel
# device info
# ephemeral messaging
# device history

# Your app's Slack bot user token
SLACK_BOT_TOKEN = os.environ["SLACK_BOT_TOKEN"]
SLACK_VERIFICATION_TOKEN = os.environ["SLACK_VERIFICATION_TOKEN"]

# Slack client for Web API requests
slack_client = SlackClient(SLACK_BOT_TOKEN)

# Flask web server for incoming traffic from Slack
app = Flask(__name__)

# Create new SQLite db
device_db = 'device_db.sqlite'

# Open a connection to an SQLite db file
conn = sqlite3.connect(device_db)
cur = conn.cursor()

# Create a new SQLite table with 6 columns
cur.execute('''CREATE TABLE IF NOT EXISTS device (name text NOT NULL, \
                                                    value text PRIMARY KEY, \
                                                    mdn integer, \
                                                    os_version text NOT NULL, \
                                                    model_type text NOT NULL, \
                                                    assignee text NOT NULL)''')
conn.commit()

# Insert values
cur.execute("INSERT OR REPLACE INTO device (name, value, mdn, os_version, model_type, assignee) \
            VALUES ('Gandalf', 'gandalf', '', 'iOS 10.3', 'iPhone 6s+', 'QA')")

# Dictionary to store device checkouts. To-do: an actual key-value store
# DEVICE_LIST = {
#     'Gandalf' : {'text': 'Gandalf','value': 'gandalf','os_version': 'iOS 10.3','model_type;': 'iPhone 6s+','assignee': 'QA'},
#     'Frodo' : {'text': 'Frodo','value': 'frodo','os_version': 'iOS 10.3','model_type;': 'iPhone 6s+','assignee': 'QA'},
#     'Bilbo' : {'text': 'Bilbo','value': 'bilbo','os_version': 'iOS 10.3','model_type;': 'iPhone 6s+','assignee': 'QA'}
#             }   
    

@app.route("/device", methods=["POST"])
def device():
    # if not is_request_valid(request):
    #     abort(400)

    request_text = request.form['text']
    if 'help' in request_text:
        return jsonify(
            {
                'text': 'Use `/device checkout` to open a dialog and check out a device. \n Use `/device search` to see who currently has the device. \n Use `/device history` to view a history of the device\'s checkouts. \n Use `/device info` to view device info like OS version, model type, etc. \n Use `/device help` to pull up this menu.'
            }
        )

    if 'checkout' in request_text:
        slack_client.api_call(
        "chat.postMessage",
        channel="#device_checkout",
        text="check out a device :iphone:",
        attachments=[
            {
            "text": "",
            "callback_id": "device_checkout_form",
            "color": "#3AA3E3",
            "attachment_type": "default",
            "actions": [{
                "name": "device_checkout_prompt",
                "text": ":iphone: Check out a phone",
                "type": "button",
                "value": "device_checkout_prompt"
                }]
            }]
        )
        return make_response("", 200)
    
    elif 'search' in request_text:
        slack_client.api_call(
        "chat.postMessage",
        channel="#device_checkout",
        text="",
        attachments=[
            {
            "text": "Which device do you want to look up?",
            "callback_id": "device_lookup",
            "color": "#3AA3E3",
            "attachment_type": "default",
            "actions": [{
                    "name": "device_lookup_list",
                    "text": "Pick a device...",
                    "type": "select",
                    "options": [
                        {
                            "text": "Bilbo",
                            "value": "bilbo"
                        },
                        {
                            "text": "Frodo",
                            "value": "frodo"
                        },
                        {
                            "text": "Gandalf",
                            "value": "gandalf"
                        },
                        {
                            "text": "Sauron",
                            "value": "sauron"
                        },
                        {
                            "text": "Treebeard",
                            "value": "treebeard"
                        }
                    ]
                }]}]
        )
        return make_response("", 200)


@app.route("/slack/message_actions", methods=["POST"])
def message_actions():
    # Parse the request payload
    message_action = json.loads(request.form["payload"])
    user_id = message_action["user"]["id"]

    if message_action["callback_id"] == "device_checkout_form":
        # Show the checkout dialog to the user
        open_dialog = slack_client.api_call(
            "dialog.open",
            trigger_id=message_action["trigger_id"],
            dialog={
                "title": "Check out a device",
                "submit_label": "Submit",
                "callback_id": user_id + "device_checkout_form",
                "elements": [
                    {
                        "label": "Device",
                        "type": "select",
                        "name": "device",
                        "placeholder": "Select a device",
                        "options": [
                            {
                                "label": "Frodo",
                                "value": "frodo"
                            },
                            {
                                "label": "Bilbo",
                                "value": "bilbo"
                            },
                            {
                                "label": "Gandalf",
                                "value": "gandalf"
                            },
                            {
                                "label": "Sauron",
                                "value": "sauron"
                            }
                        ]
                    },
                    {
                        "label": "Assignee",
                        "type": "select",
                        "name": "assignee",
                        "placeholder": "Who is checking out this device?",
                        "data_source": "users"
                    }
                ]
            }
        )

        print(open_dialog)

    elif message_action["type"] == "dialog_submission":
        # Parse the request payload
        form_json = json.loads(request.form["payload"])

        # Check to see what the user's selections were
        selected_device = form_json["submission"]["device"]
        assignee_id = form_json["submission"]["assignee"]

        # Match the assignee's user id with their name
        api_call = slack_client.api_call("users.list")
        if api_call.get('ok'):
            # retrieve all users
            users = api_call.get('members')
            for user in users:
                if 'name' in user and user.get('id') == assignee_id:
                    assignee_name = user['name']
            
        # # Create a new checkout for this user in the DEVICE_LIST dictionary
        # DEVICE_LIST[selected_device] = {
        #     "assignee": assignee_name
        # }
        # DB: Update existing entry - to be used with `/device checkout`
        conn = sqlite3.connect(device_db)
        cur = conn.cursor()
        cur.execute('UPDATE device SET assignee=? WHERE value=?', (assignee_name, selected_device[0]))
        print(selected_device, "is supposed to be checked out now to ", assignee_name)
        conn.commit()
        conn.close()

        # Confirm checkout
        conn = sqlite3.connect(device_db)
        cur = conn.cursor()
        for a,d in cur.execute("SELECT assignee,name FROM device WHERE value=?", (selected_device,)):
            new_assignee = a
            assigned_device = d
            print(assigned_device, " is now checked out to ", new_assignee)
        
        slack_client.api_call(
            "chat.postMessage",
            channel="#device_checkout",
            ts="message_ts",
            # text=selected_device+" is checked out to "+assignee_name,
            text='device checked out',
            attachments=[]
        )
    
    elif message_action["actions"][0]["name"] == "device_lookup_list":
         # Parse the request payload
        form_json = json.loads(request.form["payload"])
        
        # Check to see what the user's selection was
        selected_device = form_json["actions"][0]["selected_options"][0]["value"]
        
        # DB: Query contents of a particular column for rows that match a certain value - to be used with `/device search`
        conn = sqlite3.connect(device_db)
        cur = conn.cursor()
        d_assignee = ''
        for row in cur.execute("SELECT assignee FROM device WHERE value=?", (selected_device[0],)):
            d_assignee = row
            asdf = "new assignee is"+d_assignee
            print(asdf)

        # Match the assignee for the device selection
        # for d in DEVICE_LIST.values():
        #     print(d['value'])
        #     if d['value'] == selected_device:
        #         assignee = d['assignee']

        # # # if DEVICE_LIST[selected_device] == KeyError:
        # # #     return("see QA")
        # # # else:
        message = selected_device+" is checked out to "+d_assignee
        return(message)


        # DB: Query contents of all columns for row that match search value in 1 column - to be used with `/device info`
        # d = ({},).selected_device
        # for row in cur.execute("SELECT * FROM device WHERE value=?", d):
        #     print(row)

    return make_response("", 200)

# Commit changes to db
conn.commit()

# Close connection to db file
# conn.close()

if __name__ == "__main__":
    app.run(port=4040)
